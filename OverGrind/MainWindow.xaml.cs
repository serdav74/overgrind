﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro;

namespace OverGrind
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Entity E = new Entity();
            E.AddItem(new Item(ItemTemplates.items["iron-bar"]), 66);
            E.AddItem(new Item(ItemTemplates.items["iron-bar"]), 666);
            E.RemoveItem("iron-bar", 132);
            E.AddItem(new Item(ItemTemplates.items["iron-bar"]), 600);
            E.RemoveItem("iron-bar", 100);

            E.AddItem(new Item(ItemTemplates.items["iron-ring"]), 5);
            E.AddItem(new Item(ItemTemplates.items["iron-ring"]), 2);
            E.RemoveItem("iron-ring", 6);

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            HeroCreateUI form = new HeroCreateUI();
            form.Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            HeroOverallWindow form = new HeroOverallWindow();
            
        }
    }
}
