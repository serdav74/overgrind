﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverGrind
{
    static class Functions
    {
        public static string ItemQualityNames(ItemQuality q)
        {
            throw new NotImplementedException();
        }

        public static double[] ResistancePercentages = new double[]
        {
            0,
            0.03,
            0.05,
            0.07,
            0.09,
            0.11,
            0.13,
            0.15,
            0.165,
            0.18,
            0.195,
            0.21,
            0.22,
            0.23,
            0.24,
            0.25,
            0.2575,
            0.265,
            0.2725,
            0.28,
            0.285,
            0.29,
            0.295,
            0.3,
            0.305,
            0.31,
            0.315,
            0.32,
            0.325,
            0.33,
            0.335,
            0.34,
            0.345,
            0.35,
            0.353,
            0.356,
            0.359,
            0.362,
            0.365,
            0.368,
            0.371,
            0.374,
            0.377,
            0.38,
            0.383,
            0.386,
            0.389,
            0.391,
            0.394,
            0.397,
            0.4,
            0.402,
            0.404,
            0.406,
            0.408,
            0.41,
            0.412,
            0.414,
            0.416,
            0.418,
            0.42
        };

        public static double ResistancePointsToPercentage(int x)
        {
            if (x >= ResistancePercentages.Length)
            {
                x = ResistancePercentages.Length - 1;
            }
            if (x < 0) throw new ArgumentException();
            return ResistancePercentages[x];
        }
    }
    static class MathFunctions
    {
        public static int ValueOrBounds(this int i, int a, int b)
        {
            if (a > b) { int t = a; a = b; b = t; }
            return (i < a) ? (a) : ((i > b)?(b):i);
        }
    }
}
