﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverGrind
{
    class Entity
    {
        public string Name { get; set; }
        public string Title { get; set; }

        #region constants
        const int BASEHPMAX = 100;
        const int BASEMPMAX = 20;
        const int BASESTR = 2;
        const int BASEAGI = 2;
        const int BASEINT = 2;
        const int BASELUCK = 2;

        const int BASEATTACK = 0;
        const int BASEDEFENSE = 1;

        readonly Dictionary<MagicType, double> BASEMAGICRESISTANCES = new Dictionary<MagicType, double>()
        {
            [MagicType.Light] = 0.0,
            [MagicType.Dark] = 0.0,
            [MagicType.Fire] = 0.0,
            [MagicType.Water] = 0.1,
            [MagicType.Air] = 0.1,
            [MagicType.Earth] = 0.0,
            [MagicType.Energy] = 0.0
        };
        #endregion

        #region fields 1
        int hp;
        int mp;
        long xp;
        int lvl;
        int _str;
        int _agi;
        int _int;
        int _luck;
        #endregion

        public static long LevelCost(int lvl) { return (long)Math.Round(50 + 20 * lvl + 0.7 * Math.Pow(lvl, 1.337) + 0.3 * Math.Pow(lvl, 1.618)); }
        public static double ResistancePointsToPercentage(int points) { return Functions.ResistancePointsToPercentage(points); }

        public int AttributePoints { get; set; }
        #region HP, MP
        public int HP
        {
            get { return hp; }
            set { hp = value.ValueOrBounds(0, HPMax); }
        }

        public int HPMax
        {
            get { return (int)Math.Round((BASEHPMAX + Equip.Sum(x => x.HPMaxBonus))*(1 + Equip.Sum(x => x.HPMaxMultiplier)) ); }
        }

        public int MP
        {
            get { return mp; }
            set { mp = value.ValueOrBounds(0, MPMax); }
        }

        public int MPMax
        {
            get { return (int)Math.Round((BASEMPMAX + Equip.Sum(x => x.MPMaxBonus)) * (1 + Equip.Sum(x => x.MPMaxMultiplier))); }
        }
        #endregion
        #region XP-related stuff
        public int Level { get { return lvl; } }
        public long XPRequired { get { return LevelCost(Level + 1); } }
        public long XP { get { return xp; } }
        public void AddXP(long amount)
        {
            xp += amount;
            int i = 0;
            while (xp > LevelCost(lvl + 1))
            {
                lvl++;
                i++;
                xp -= LevelCost(lvl); //as we already increased lvl
            }
            if (i > 0) { LevelledUp?.Invoke(this, new LevelUpEventArgs() { LevelAmount = i }); }
        }

        public delegate void EntityLevelledUp(object sender, LevelUpEventArgs e);
        public event EntityLevelledUp LevelledUp;
        #endregion
        #region Attributes
        public int Strength { get { return Math.Max(0, BASESTR + _str + Equip.Sum(x => x.GetAttributeBonus(Attribute.Strength)) + Buffs.Sum(x => (x.Type == MagicType.Fire && x.Positive)?(x.Modifiers[0]):0) - Buffs.Sum(x => (x.Type == MagicType.Energy && !x.Positive)?(x.Modifiers[0]):(0))); } }
        public int Agility { get { return Math.Max(0, BASEAGI + _agi + Equip.Sum(x => x.GetAttributeBonus(Attribute.Agility)) + Buffs.Sum(x => (x.Type == MagicType.Air && x.Positive)?(x.Modifiers[0]):0) - Buffs.Sum(x => (x.Type == MagicType.Water && !x.Positive)?(x.Modifiers[0]):0) ); } }
        public int Intellect { get { return Math.Max(0, BASEINT + _int + Equip.Sum(x => x.GetAttributeBonus(Attribute.Intellect)) + Buffs.Sum(x => (x.Type == MagicType.Water && x.Positive)?(x.Modifiers[0]):0)); } }
        public int Luck { get { return Math.Max(0, BASELUCK + _luck + Equip.Sum(x => x.GetAttributeBonus(Attribute.Luck)) + Buffs.Sum(x => (x.Type == MagicType.Energy && x.Positive)?(x.Modifiers[0]):0) - Buffs.Sum(x => (x.Type == MagicType.Energy && !x.Positive) ? (x.Modifiers[1]) : 0)); } }

        public int GetAttribute(Attribute a)
        {
            switch (a)
            {
                case Attribute.Strength: return Strength;
                case Attribute.Agility: return Agility;
                case Attribute.Intellect: return Intellect;
                case Attribute.Luck: return Luck;
                default: return 0;
            }
        }
        public bool AddAttribute(Attribute a)
        {
            if (AttributePoints <= 0) { return false; }
            AttributePoints -= 1;
            switch (a)
            {
                case Attribute.Strength: _str++; return true;
                case Attribute.Agility: _agi++; return true;
                case Attribute.Intellect: _int++; return true;
                case Attribute.Luck: _luck++; return true;
                default: return false;
            }
        }
        #endregion
        public int Defense { get { return (int)Math.Round((BASEDEFENSE + Equip.Sum(x => x.DefenseBonus))*(1 + Equip.Sum(x => x.DefenseMultiplier)) + Buffs.Sum(x => (x.Type == MagicType.Earth && x.Positive)?(x.Modifiers[0]):0) + Buffs.Sum(x => (x.Type == MagicType.Earth && !x.Positive)?(x.Modifiers[0]):0)); } }
        public int Attack {
            get {
                ItemType WeaponType = Equip.First(x => x.EquipSlot == ItemEquipSlot.RightHand).Type;
                Attribute CorrespondingAttribute = WeaponBonusStat[WeaponType];
                double AttackAttributeMult = 1 + WeaponAttackMultFromAttribute(CorrespondingAttribute);

                if (!Equip.Any(x => x.EquipSlot == ItemEquipSlot.RightHand)) AttackAttributeMult = 1;

                int AttackBonus = Equip.Sum(x => x.AttackBonus);

                int BuffBonus = Buffs.Sum(x => (x.Type == MagicType.Dark && x.Positive) ? (x.Modifiers[0]) : 0);

                BuffBonus -= Buffs.Sum(x => (x.Type == MagicType.Earth && !x.Positive) ? (x.Modifiers[1]) : 0);

                return (int)Math.Round((BASEATTACK + AttackBonus) * AttackAttributeMult);
            }
        }

        static Dictionary<ItemType, Attribute> WeaponBonusStat = new Dictionary<ItemType, Attribute> { [ItemType.Sword] = Attribute.Strength, [ItemType.Bow] = Attribute.Agility, [ItemType.Staff] = Attribute.Intellect };
        double WeaponAttackMultFromAttribute(Attribute a) { return 0.01 * GetAttribute(a); }


        public Dictionary<MagicType, int> MagicResistancePoints = new Dictionary<MagicType, int>();

        public double GetMagicResistance(MagicType t)
        {
            return BASEMAGICRESISTANCES[t] + ResistancePointsToPercentage(MagicResistancePoints[t]);
        }

        //
        // <=== ITEMS ===>
        //

        public Dictionary<ItemEquipSlot, int> EquipLimits = new Dictionary<ItemEquipSlot, int>
        {
            [ItemEquipSlot.None] = 0,
            [ItemEquipSlot.Head] = 1,
            [ItemEquipSlot.Neck] = 1,
            [ItemEquipSlot.Body] = 1,
            [ItemEquipSlot.LeftHand] = 1,
            [ItemEquipSlot.RightHand] = 1,
            [ItemEquipSlot.Foots] = 1,
            [ItemEquipSlot.Finger] = 4,
            [ItemEquipSlot.Pocket] = 2
        };

        public ObservableCollection<Item> Backpack = null;
        public ObservableCollection<Item> Equip = null;

        public void AddItem(Item item, int amount)
        {
            if (item.IsStackable)
            {
                Item match = Backpack.FirstOrDefault(x => x.ID == item.ID);
                if (match != null)
                {
                    match.Amount += amount;
                }
                else
                {
                    Backpack.Add(new Item(item) { Amount = amount });
                }
            }
            else
            {
                for (int i = 0; i < amount; i++)
                {
                    Backpack.Add(new Item(item));
                }
            }
        }
        public void AddItem(Item item)
        {
            int a = item.Amount;
            item.Amount = 1;
            AddItem(item, a);
        }

        public bool HasItem(string id, int amount = 1)
        {
            if (amount < 0) { throw new ArgumentException(); }
            int ta = Backpack.Where(x => x.ID == id).Sum(x => x.Amount);
            return (ta >= amount);
        }

        public bool RemoveItem(string id, int amount = 1)
        {
            if (HasItem(id, amount))
            {
                if (ItemTemplates.items[id].IsStackable)
                {
                    Backpack.First(x => x.ID == id).Amount -= amount;
                }
                else
                {
                    List<Item> matching = Backpack.Where(x => x.ID == id).ToList();
                    int i = 0;
                    while (amount > 0)
                    {
                        matching[i].Amount = 0;
                        amount -= 1;
                        i++;
                    }
                }
                for (int i = Backpack.Count-1; i >= 0; i--)
                {
                    if (Backpack[i].Amount <= 0) Backpack.RemoveAt(i);
                }
                return true;
            } else
            {
                throw new InvalidOperationException();
            }
        }


        #region Buffs

        public List<Buff> Buffs = new List<Buff>();

        #endregion

        public bool IsMob = false;
        public MobDrop? MobDrop = null;

        public Entity()
        {
            Name = "Вася";
            Title = "Чебурек";
            xp = 0;
            AttributePoints = 2;
            lvl = 0;
            Backpack = new ObservableCollection<Item>() { new Item(ItemTemplates.items["iron-ring"]) };
            Equip = new ObservableCollection<Item>();

        }

        public Entity(string Name, string Title)
        {
            this.Name = Name;
            this.Title = Title;

            xp = 0;
            AttributePoints = 7;
            lvl = 0;
            Backpack = new ObservableCollection<Item>();
            Equip = new ObservableCollection<Item>();

            MagicResistancePoints = new Dictionary<MagicType, int>();
            foreach (MagicType m in Enum.GetValues(typeof(MagicType)))
            {
                MagicResistancePoints.Add(m, 0);
            }

            IsMob = false;
        }



        public override string ToString()
        {
            return Name + " " + Title + " [" + Level + "]";
        }
    }
    
    public enum Attribute
    {
        Strength, Agility, Intellect, Luck
    }

    public enum MagicType
    {
        Light, Dark, Fire, Water, Earth, Air, Energy
    }
    class LevelUpEventArgs : EventArgs
    {
        public int LevelAmount { get; set; }
    }

    struct MobDrop
    {
        public List<Item> Items;
        public long XP;
        public long Gold;
    }
}
