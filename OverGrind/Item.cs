﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverGrind
{
    class Item
    {
        public readonly string ID;

        public string Name { get; set; }
        public ItemEquipSlot EquipSlot { get { return equipslot; } }
        public ItemType Type { get { return type; } }
        public ItemQuality Quality { get { return quality; } }
        public string Description { get; set; }

        const long BASEDURABILITY = 800;

        public bool IsStackable = false;
        public int Amount = 1;

        ItemEquipSlot equipslot;
        ItemType type;
        ItemQuality quality;
        long durability;

        public long Durability { get { return durability; } set { durability = value; if ((durability < 0) && (quality > ItemQuality.Usual)) { durability = BASEDURABILITY; quality -= 1; } } }

        Dictionary<Attribute, int> attributeBonus;
        Dictionary<MagicType, double> magicResistBonus;
        int hpmaxbonus;
        int mpmaxbonus;
        int defensebonus;
        int attackbonus;

        double hpmaxmult;
        double mpmaxmult;
        double defensemult;

        public int GetAttributeBonus(Attribute t) => (attributeBonus.ContainsKey(t))?attributeBonus[t]:0;
        public double GetMagicResistBonus(MagicType m) => (magicResistBonus.ContainsKey(m))?magicResistBonus[m]:0;
        public int HPMaxBonus { get { return hpmaxbonus; } }
        public int MPMaxBonus { get { return mpmaxbonus; } }
        public int DefenseBonus { get { return defensebonus; } }
        public int AttackBonus { get { return attackbonus; } }
        public double HPMaxMultiplier { get { return hpmaxmult; } }
        public double MPMaxMultiplier { get { return mpmaxmult; } }
        public double DefenseMultiplier { get { return defensemult; } }

        public long? BuyPrice { get; set; }
        public long? SellPrice { get; set; }

        public override string ToString()
        {
            if (IsStackable)
            {
                return Name + " x" + Amount.ToString();
            }
            else
            {
                return Name;
            }
        }

        public Item(string id, string name, ItemType type, ItemEquipSlot equipslot = ItemEquipSlot.None, ItemQuality quality = ItemQuality.Usual, 
            string description = null, bool stackable = false, Dictionary<Attribute, int> attrbonus = null, Dictionary<MagicType, double> magresistbonus = null, 
            int hpmaxbonus = 0, int mpmaxbonus = 0, int defensebonus = 0, int attackbonus = 0, double hpmaxmult = 0, double mpmaxmult = 0, double defensemult = 0)
        {
            ID = id;
            Name = name;
            this.type = type;
            this.equipslot = equipslot;
            this.quality = quality;
            Description = description ?? "";
            IsStackable = stackable;
            attributeBonus = attrbonus ?? new Dictionary<Attribute, int>();
            magicResistBonus = magresistbonus ?? new Dictionary<MagicType, double>();
            this.hpmaxbonus = hpmaxbonus;
            this.mpmaxbonus = mpmaxbonus;
            this.defensebonus = defensebonus;
            this.attackbonus = attackbonus;
            this.hpmaxmult = hpmaxmult;
            this.mpmaxmult = mpmaxmult;
            this.defensebonus = defensebonus;
        }

        public Item(Item i, string NewName = null, ItemQuality? NewQuality = null)
        {
            ID = i.ID;
            Name = NewName ?? i.Name;
            equipslot = i.equipslot;
            type = i.type;
            quality = NewQuality ?? i.quality;
            durability = i.durability;
            IsStackable = i.IsStackable;

            Description = i.Description;

            attributeBonus = new Dictionary<Attribute, int>(i.attributeBonus);
            magicResistBonus = new Dictionary<MagicType, double>(i.magicResistBonus);

            hpmaxbonus = i.hpmaxbonus;
            mpmaxbonus = i.mpmaxbonus;
            defensebonus = i.defensebonus;
            attackbonus = i.attackbonus;

            hpmaxmult = i.hpmaxmult;
            mpmaxmult = i.mpmaxmult;
            defensemult = i.defensemult;

            BuyPrice = i.BuyPrice;
            SellPrice = i.SellPrice;
        }
    }

    public enum ItemQuality
    {
        Usual, Interesting, Rare, Mythical, Epic, Legendary
    }

    public enum ItemType
    {
        Trash, Resource,
        Sword, Shield, Staff, Bow, Helmet, Necklace, Armor, Boots, Ring, Talisman,
        Consumable
    }

    public enum ItemEquipSlot
    {
        None, LeftHand, RightHand, Head, Neck, Body, Foots, Finger, Pocket
    }

}
