﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverGrind
{
    static class ItemTemplates
    {
        public static Dictionary<string, Item> items = new Dictionary<string, Item>(10);

        static ItemTemplates()
        {
            string id = "iron-bar";
            Item i = new Item(id, "Слиток железа", ItemType.Resource, ItemEquipSlot.None, ItemQuality.Usual, "", true) { BuyPrice = 20, SellPrice = 10 };
            items.Add(id, i);

            id = "iron-ring";
            i = new Item(id, "Железное кольцо", ItemType.Ring, ItemEquipSlot.Finger, ItemQuality.Usual, "Обычное железное кольцо. Слегка жмёт.", magresistbonus: new Dictionary<MagicType, double> { [MagicType.Energy] = 0.01 });
            items.Add(id, i);


        }
    }
}
