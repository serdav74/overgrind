﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverGrind
{
    class Buff
    {
        public MagicType Type;
        public int? Duration;
        public int[] Modifiers;
        public bool Positive;

        public void Action(Entity Target)
        {
            if (Positive)
            {
                switch (Type)
                {
                    case MagicType.Light:
                        Target.HP += Modifiers[0];
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (Type)
                {
                    case MagicType.Fire:
                        Target.HP -= Modifiers[0];
                        break;
                    default:
                        break;
                }
            }


            if (Duration.HasValue)
            {
                Duration -= 1;
            }
            else //if Duration == null, buff is instant
            {
                Duration = 0;
            }
        }

        public Buff(BuffTemplate te)
        {

        }

    }
    //    Light, Dark, Fire, Water, Earth, Air, Energy

    //MOD or MOD[0] stands for Modifier[0]
    //MOD[1] - Modifier[1]

    //Positive effects:
    //Light: heal
    //Dark: bonus to attack
    //Fire: bonus to STR
    //Water: bonus to INT
    //Earth: bonus to DEF
    //Air: bonus to AGI
    //Energy: bonus to LUCK
    //
    //Negative effects:
    //Light: Blindness = 1+MOD% damage received
    //Dark: Darkness = MOD% chance to miss
    //Fire: Fire = MOD damage per second, ignores armor
    //Water: Slowness = -MOD to AGI
    //Earth: +MOD armor, -MOD[2] attack, can't evade attacks
    //Air: Stun, can't evade
    //Energy: Weakness = -MOD to STR, -MOD[2] to LUCK
    struct BuffTemplate
    {
        public MagicType Type;
        //null duration = instant
        public int? Duration;
        public int[] Modifiers;
        public BuffTargets Targets;
        public bool Positive;
    }

    enum BuffTargets
    {
        Self,
        Team,
        OneFromTeam,
        OneFromTeamExceptSelf,

        Enemy,
        EnemyTeam
    }
    
}
