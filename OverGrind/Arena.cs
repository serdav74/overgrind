﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverGrind
{
    class Arena
    {
        Random r = new Random();

        public List<Entity> Team1;
        public List<Entity> Team2;

        public int Turn = 0;
        public bool AlliesTurn;

        public delegate void ArenaActionLogged(object sender, ArenaActionLoggedEventArgs e);
        public event ArenaActionLogged ActionLogged;

        void Log(string message) { ActionLogged?.Invoke(this, new ArenaActionLoggedEventArgs() { Message = message }); }

        public void Attack(Entity Attacker, List<Entity> Targets, bool Weapon = true)
        {
            foreach (Entity Target in Targets)
            {
                if (Weapon)
                {
                    //miss check

                    bool CanMiss = Attacker.Buffs.Any(x => x.Type == MagicType.Dark && !x.Positive);
                    bool Missed = false;

                    if (CanMiss)
                    {
                        double chance = Attacker.Buffs.Max(x => x.Modifiers[0])/100.0;
                        if (chance < r.NextDouble())
                        {
                            Missed = true;
                        }
                    }


                    //Evasion check

                    bool Evaded = false;

                    if (!Missed)
                    {
                        bool CanEvade = (Target.Agility > Attacker.Agility);

                        if (Target.Buffs.Any(x => (x.Type == MagicType.Earth && !x.Positive) || (x.Type == MagicType.Air && !x.Positive))) CanEvade = false;

                        if (CanEvade)
                        {
                            //depending on how close target AGI to attacker's 2xAGI
                            double chance = (Target.Agility - Attacker.Agility) / ((double)Attacker.Agility);

                            if (chance < r.NextDouble())
                            {
                                Evaded = true;
                            }

                        }

                    }

                    int dmg = 0;
                    bool ArmorPierced = false;

                    if (!Missed && !Evaded)
                    {
                        int dmg_raw = Attacker.Attack;
                        //int dmg = Attacker.Attack - Target.Defense;

                        if (Target.Buffs.Any(x => x.Type == MagicType.Light && !x.Positive))
                        {
                            dmg_raw += (int)Math.Round(dmg_raw * (double)Target.Buffs.Sum(x => (x.Type == MagicType.Light) ? (x.Modifiers[0]) : 0)/100.0);
                        }

                        int def = Target.Defense;

                        def = (int)Math.Round(def * r.Next(90, 100+(int)(Math.Max(30, Target.Luck/1.5))) / 100.0);

                        ArmorPierced = (dmg_raw >= def);

                        if (ArmorPierced) dmg = dmg_raw - def; else dmg = 0;



                    }


                    if (Missed) Log(string.Format("{0} пытается атаковать {1}, но промахивается.", Attacker.Name, Target.Name));

                    if (Evaded) Log(string.Format("{0} пытается атаковать, но {1} уворачивается.", Attacker.Name, Target.Name));

                    if (!ArmorPierced) Log(string.Format("{0} пытается атаковать, но не пробивает броню {1}.", Attacker.Name, Target.Name));

                    if (ArmorPierced)
                    {
                        Target.HP -= dmg;

                        Log(string.Format("{0} атакует {1} и наносит {2} физ. урона.", Attacker.Name, Target.Name, dmg));
                    }



                }






            }
        }

        public Arena(Entity Player)
        {
            Team1 = new List<Entity>() { Player };
            Team2 = new List<Entity>() { new Entity() };
        }
    }

    class ArenaActionLoggedEventArgs : EventArgs
    {
        public string Message { get; set; }
    }

    static class AI
    {
        static Random r = new Random();
        public static void MobTurn(this Entity entity, Arena a, List<Entity> enemies)
        {
            Entity Target = PickEnemy(enemies);

            a.Attack(entity, new List<Entity>() { Target }, true);
        }

        static Entity PickEnemy(List<Entity> enemies)
        {
            if (enemies.Count == 1) { return enemies.First(); }
            List<WeightedRandomVariant> ws = new List<WeightedRandomVariant>(enemies.Count);

            double sum = 0;
            double c = 0;
            foreach (Entity e in enemies)
            {
                c = e.Level / 2 + (10.0 / e.Luck);
                ws.Add(new WeightedRandomVariant() { o = e, p = c });
                sum += c;
            }

            sum = r.NextDouble() * sum;

            for (int i = 0; i < ws.Count; i++)
            {
                sum -= ws[i].p;

                if (sum <= 0) { return (ws[i].o as Entity); }
            }

            return enemies.Last();

        }

        protected struct WeightedRandomVariant
        {
            public object o;
            public double p;
        }
    }
}
